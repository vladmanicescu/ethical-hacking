#!/usr/bin/python3
import scapy.all as scapy
def scanner(ip):
    arp_request = scapy.ARP(pdst = ip)
    #print(arp_request.summary())
    #scapy.ls(scapy.ARP)
    #print(arp_request.show())
    broadcast = scapy.Ether(dst = 'ff:ff:ff:ff:ff:ff')
    arp_request_broadcast = broadcast/arp_request
    print(arp_request_broadcast.show())
    answered,unanswered = scapy.srp(arp_request_broadcast,timeout=1)
    print(answered.summary())
    #scapy.ls(scapy.Ether)

scanner('192.168.56.0/24')
