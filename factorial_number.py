#0!=1
#1!=1
#2!=1*2 = 2
#3!=1*2*3 = 6
#4!=1*2*3*4 = 24
#5!=1*2*3*4*5=120

def factorial_number(number):
    factorial = 1
    for i in range(1,number+1):
        factorial = factorial*i
    return factorial


def recursive_factorial_number(number):
    if (number == 1):
        return 1
    else:
        return number * recursive_factorial_number(number-1)


#print(factorial_number(6))
#print(recursive_factorial_number(4))

#number = 5
# 5 == 1? -> nu -> else -> return 5 *  4 * 3 * 2 * 1=120
#                                      4 ==1 ? -> nu -> return 4 * recursive_factorial_number(3)
#                                      3 ==1? -> nu -> return 3 * recursive_factorial_number(2)
#                                      2 == 1? -> nu -> return 2 * recursive_factorial_number(1)
#                                      1 == 1? -> da -> return 1
