def my_function(name):
   print("Hello " + name + '!')
   return 1

def average(number1,number2):
    ''' Simple function that prints the average of two numbers '''
    average = (number1 + number2)/2
    #print(average)
    return average


#print(range.__doc__)
#a = my_function('Hubert')
#print(a)
#print(4 + average(3,5))
#a = average(3,5)
#print(a)
print(average(3,5))