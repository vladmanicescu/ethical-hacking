def ispalindrom(a):

    '''Simple functions that checks if a number is palindrom or not
    A palindrom number has the same value when read from left to right or from right to left
    '''

    if a == a[::-1]:
        return True
    else:
        return False

print("-" * 15, 'user input mode mode', '-' * 15)
a = input("Please enter number:")
print(a,ispalindrom(a))
print("-"*15,'range mode','-'*15)

for number in range(10,100):
    number = str(number)
    print(number,ispalindrom(number))


print("-"*15,'file mode','-'*15)
with open("file.txt",'r') as my_file:
    a = my_file.read()
    print(a, ispalindrom(a))