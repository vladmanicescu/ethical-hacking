#!/usr/bin/python
import subprocess
import optparse
import re
def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-i",'--interface',dest="interface",help="Interface to change its mac address")
    parser.add_option("-m","--mac-address",dest="macAddress",help="mac address to be used for the change")
    (options,arguments) = parser.parse_args()
    if not options.interface:
        parser.error("[-] Please specify an interface use --help for info.")
    elif not options.macAddress:
        parser.error("[-] Please specify a mac Address use --help for info.")
    return options,arguments

def get_argument (argument):
    returned_argument = input(argument)
    return returned_argument

def change_mac(interface,newMac):
    print(f"[+] Changing mac address for {interface} to {newMac}")
    command_list = [["ifconfig", interface, "down"], ["ifconfig", interface, "hw", "ether", newMac],
                    ["ifconfig", interface, "up"]]
    for item in command_list:
        print(item)
        subprocess.call(item)

def validate_mac_address(given_mac, calculated_mac):
    if given_mac == calculated_mac:
        print("[+] Mac address changed.")
    else:
        print("[-] Mac address not changed")

def get_mac_address(interface):
    ifconfig_output = subprocess.check_output(["ifconfig", interface]).decode("utf-8")
    mac_address_search_result = re.search(r'((\w{2}:){5})\w{2}', ifconfig_output)
    if mac_address_search_result.group(0):
        return mac_address_search_result.group(0)
    else:
        print("[-] There is no mac address")

#interface = get_argument("interface")
#newMac = get_argument("mac")
options,arguments = get_arguments()
interface = options.interface
newMac = options.macAddress
change_mac(interface, newMac)
current_mac = get_mac_address(interface)
validate_mac_address(newMac,current_mac)