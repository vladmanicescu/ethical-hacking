#print(range.__doc__)
def list_average(name='Vlad',list_of_numbers=[0,0,0]):

    '''Function that calculates the average value of all the elements inside a list

    Returns the string value of the name it is being called with(defaults to Vlad)
    Also returns the float value of the average of the elements of the list that it is being called with(defaults to [0,0,0])

    '''

    out_sum = 0
    for item in list_of_numbers:
        out_sum += item
    out_average = out_sum/len(list_of_numbers)
    return out_average, name

if __name__ == '__main__':

  a = [4,5,7]
  #print(list_average('Mihai',a))
  #print('The name of functions_4 script is',__name__)
  b,c = list_average()
  print(b)
  print(c)
#def range(stop,start=0,step=1)