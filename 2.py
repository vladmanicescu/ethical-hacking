a = 'Hello world!'
a = 'b'
a = " "
a = '1'
a = ""
#print(a)
#a = "El a spus 'hello' "
#print(a)
#a = "in python stringurile se definesc intre \"\" \'\' \\n !!!!! "
#a = r"C:\Users\Vlad.Manicescu\Desktop\SRE Documentation\new_folder\test"
#a = 1
#b = 2
'''
a = "abc"
b= 'cde '
c = 3
print(a + b * c)
'''

#a = "Hello world w!"
#print(a[2:7])
#print(a[::-1])
#a[1] = 'x'
#print(a)
#b = len(a)
#print(len(a))
#print(a.find("world"))
#print(a.find("w"))
#a = "I am studying Python "
#print(a.replace("Python","Java"))
#a = a.replace("Python","Java")
#print(a.lower())
#print(a.upper())
#a = a.lower()
#print(a)

#a = 'python is a nice programming language.'
#print(a.isupper())
#print(a.islower())
#print("python".isupper())
#b = a.lower()
#print(b.islower())
#print(a.lower().islower())
#a = a.capitalize()
#print(a)
#a = '1234'
#print(a.isdigit())
#a = "test"
#print(a.isalpha())
#a = "test123"
#print(a.isalnum())

'''
a = input("Va rog introduceti varsta:")
#print(type(a))
if a.isdigit():
  a = int(a)
  if a == 0:
      print("Am intrat in if")
      print("Esti nou nascut")
  elif a < 18:
      print("Am intrat in elif")
      print("Esti minor")
  elif a >= 200:
      print("Am intrat in al doilea elif")
      print("Nu mai esti printre noi")
  else:
      print("Am intrat in else")
      print("Esti major")
else:
    print("Nu ai introdus numar pozitiv...mai incearca o data")

print("End")
'''

a = input("Va rog introduceti varsta:")
#print(type(a))
if a.isdigit():
  a = int(a)
  if a < 18 and a >= 0:
      print("Esti minor")
  elif a >= 200:
      print("Nu mai esti printre noi")
  else:
      print("Esti major")

else:
    print("Nu ai introdus numar pozitiv...mai incearca o data")

print("End")
