import sys
def get_values(user_prompt):
    option = input(user_prompt)
    return option

def calculate(number1,number2,operator):
    if operator == '/':
        return number1/number2
    elif operator == '*':
        return number1 * number2
    elif operator == '-':
        return number1 - number2
    elif operator == '+':
        return number1 + number2

try:
    a = int(get_values("Please enter the first number: "))
    b = int(get_values("Please enter the second number: "))
    c = get_values("Please enter the operation you want to perform (+/-*): ")
    if c not in '/*-+':
        raise TypeError("Operator not supported")
    final_result = calculate(a, b, c)
except ValueError:
    print("Try  again...you need to enter only numbers")
    sys.exit(1)
except TypeError:
    print("You entered an operator that is not supported")
    sys.exit(2)
except ZeroDivisionError:
    print("You cannot devide by 0")
    sys.exit(-15)

print(final_result)