#!/usr/bin/python3
import scapy.all as scapy

def scan(ip):
    #create arp packet
    arp_request = scapy.ARP(pdst = ip)
    #create ethernet frame to encapsulate the packet
    broadcast = scapy.Ether(dst = 'ff:ff:ff:ff:ff:ff')
    #arp_request.pdst=ip
    #print(arp_request.summary)
    #scapy.ls(scapy.ARP)
    #print(broadcast.summary)
    #scapy.ls(scapy.Ether())
    #encapsulate arp request packet in ethernet frame
    arp_request_broadcast = broadcast/arp_request
    #send the encapsulated packet
    #answered, unanswered = scapy.srp(arp_request_broadcast,timeout = 1)
    answered  = scapy.srp(arp_request_broadcast,timeout = 1, verbose = False)[0]
    #print("IP" + "\t" * 3 +"MAC ADDRESS\n")
    #print(answered.summary())

    #########dictionary part###########
    ######################
    clients_list = []

    for element in answered:
        #print("---"*3)
        #print(element[1].show())
        #
        #print(element[1].hwsrc)
        client_dict = {"ip":element[1].psrc ,"mac":element[1].hwsrc}
        clients_list.append(client_dict)
    #print(clients_list)
    return clients_list
def print_result(results_list):
    print("IP" + "\t" * 3 + "MAC ADDRESS\n")
    for client in results_list:
        print("---"*3)
        print(client["ip"] + '\t' * 2 + client["mac"])

    #print('arp reauest broadcast summary',arp_request_broadcast.summary())
    #print('arp request show',arp_request.show)()
    #print('broadcast show',broadcast.show())
    #print('arp request broadcast show',arp_request_broadcast.show())
clients = scan("192.168.56.1/24")
print_result(clients)
