#!/usr/bin/python3
import subprocess
import optparse
import re

def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--interface",dest="interface",help="Interface to change its mac address")
    parser.add_option("-m", "--mac-address", dest="macAddress", help="New mac address to be used")
    (options, arguments) = parser.parse_args()
    return options,arguments

def change_mac(interface, mac_address):
    command_if_down = ["ifconfig", interface, "down"]
    command_if_change_mac = ["ifconfig", interface, "hw", "ether" ,mac_address]
    command_if_up = ["ifconfig", interface ,"up"]
    list_of_cmds = [command_if_down,command_if_change_mac,command_if_up]
    for cmd in list_of_cmds:
        subprocess.call(cmd)

def get_mac(interface):
    output = subprocess.check_output(['ifconfig',interface]).decode('utf8')
    calculated_mac = re.search(r'(\w{2}:){5}\w{2}',output)
    #print(calculated_mac.group(0))
    return calculated_mac.group(0)

def validate_mac(new_mac,old_mac):
    if new_mac == old_mac:
        print("[+] Mac address changed")
    else:
        print("[-] Mac address didn't change")

#interface = input("[+] Please enter interface:")
#newMac = input("[+] Please enter new mac address:")
my_options,my_arguments = get_arguments()
interface = my_options.interface
newMac = my_options.macAddress
change_mac(interface,newMac)
actualMac = get_mac(interface)
validate_mac(newMac,actualMac)

