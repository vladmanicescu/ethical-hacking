print("-" * 15, 'user input mode mode', '-' * 15)
a = input("Please enter the number:")
if a == a[::-1]:
    print(a,"is a palindrom")
else:
    print(a,"is not a palindrom")

print("-"*15,'range mode','-'*15)
for number in range (10,100):
    if str(number) == str(number)[::-1]:
        print(number,"is a palindrom")
    else:
        print(number,"is not a palindrom")
print("-"*15,'file mode','-'*15)

with open("file.txt",'r') as my_file:
    a = my_file.read()
    if a == a[::-1]:
        print(a, "is a palindrom")
    else:
        print(a, "is not a palindrom")

