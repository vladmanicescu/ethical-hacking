import sqlite3

#a = input("Please insert hostname: ")
#b = input("Please insert ip address: ")
#c = input("Please insert number of CPUs: ")

my_connection = sqlite3.connect('servers.db')
#print(my_connection)
my_cursor = my_connection.cursor()
my_query = "select * from servers;"
my_cursor.execute(my_query)
#print(my_cursor.fetchall())
for row in my_cursor.fetchall():
    print('-'*45)
    print(row)
    print('-' * 45)

#my_query_2 = f"insert into servers VALUES('{a}','{b}',{c})"

#my_cursor.execute(my_query_2)
#my_connection.commit()

#my_querries = """insert into servers VALUES('opc3asd20','192.168.1.101',2);
#insert into servers VALUES('opc3asd25','192.168.1.105',3);
#insert into servers VALUES('opc3asd26','192.168.1.106',1);"""
#print(my_querries)

#my_cursor.executescript(my_querries)
#my_connection.commit()
with open("db_script",'r') as queryf:
    my_querries = queryf.read()
#print(my_querries)
my_cursor.executescript(my_querries)
my_connection.commit()

my_connection.close()
