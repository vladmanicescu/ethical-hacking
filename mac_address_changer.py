#!/usr/bin/python
import subprocess
def get_interface():
    interface = input("interface:")
    return interface

def get_mac():
    mac = input("new mac:")
    return mac

interface = get_interface()
newMac = get_mac()
print(f"[+] Changing mac address for {interface} to {newMac}")
command_list = [f"ifconfig {interface} down",f"ifconfig {interface} hw ether {newMac}",f"ifconfig {interface} up" ]
for item in command_list:
    subprocess.call(item,shell=True)