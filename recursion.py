#0! = 1
#1! = 1
#2! = 1*2=2
#3! = 1*2*3=6
#4!= 1*2*3*4 = 24
#5!= 1*2*3*4*5 = 120 = 3!*4*5=4!*5
#6!= 1*2*3*4*5*6= 720= 5!*6
#....................
#n! = 1*2*3*...(n-1)*n = (n-1)!*n

'''
def iterative_factorial(number):
    result = 1
    for item in range(1,number+1):
        result *= item
    return result

def recursive_factorial(number):
    if number == 1:
        return 1
    else:
        return (number * recursive_factorial(number-1))

print(recursive_factorial(5))


#number = 5 
# 5 == 1? -> no -> return (5 * 4 * 3 * 2 *  1 ) = 120
                               #number = 4 -> 4 == 1? -> no -> return(4*recursive_factorial(3))
                                                               #number = 3 -> 3 == 1? -> no -> return(3 * recursive_factorial(2))
                                                                                                      #number =2 -> 2 == 1? -> no ->  return(2 * recursive_factorial(1))
'''                                                                                                                                       # number = 1 -> 1 == 1? -> yes ->  return 1
