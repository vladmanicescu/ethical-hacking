class movies:
    def __init__(self,surname,name,list_of_movies):
        '''This is the constructor of the class'''
        self.surname = surname
        self.name = name
        self.list_of_movies = list_of_movies

    def add_movie(self,movie):

        '''
        This is a method that adds a movie to the actor's list of movies.
        The parameter should be passed as string and it will be added to the movies list.
        '''
        self.list_of_movies.append(movie)

    def presentation(self):
        print(f'My name is {self.surname} {self.name}. I played in the following movies:')
        for i in self.list_of_movies:
            print(f'[+] {i}')

    def save_object_to_file(self):
        filename = self.surname + '_' + self.name + '.txt'
        with open(filename,'a') as my_file:
            my_file.write(self.surname + ' ' + self.name + ':')
            for movie in self.list_of_movies:
                my_file.write("\n" + movie)

#main

actor1 = movies("Will","Smith",["I am legend","Suicide Squad"])
actor1.add_movie("Men in black")
actor1.save_object_to_file()
actor1.add_movie("I Robot")
actor1.save_object_to_file()
#actor1.presentation()
#print(actor1.__dict__['list_of_movies'][-1])

#print(actor1.add_movie.__doc__)
#print(actor1.__dict__)
#print(range.__doc__)