def my_first_function():
    """This is just a test function.
    It requires no arguments.
    """
    print("I am inside the first function")

def second_function():
    '''This is another docstring example'''
    print("I am inside another function")

def greetings(name, age):
    print("Hello " + name)
    print(f"You are {age} years old" )

def average(number1,number2):
    avg = (number1 + number2)/2
    suma = number1 - number2
    return avg

def list_average(list_of_numbers):
    list_avg = sum(list_of_numbers)/len(list_of_numbers)
    return list_avg

def verify_integer(number):
    if round(number) == number:
        return True
    else:
        return False


#print("I am here")
#my_first_function()
#print(my_first_function.__doc__)
#print(range.__doc__)
#print(second_function.__doc__)
#greetings(55, 33)
#print(my_first_function())

#a = my_first_function()
#print(my_first_function)
#print(a)
#print(average(3,5))
#a = average(3,5)
#print(a)

#a = [1,2,3,4]
#b = a.reverse()
#print(a,b)
#a = average(3,5)
#c = a[0]
#d = a[1]
#print(f"Media aritmetica este {c}, diferenta este{d}")
#c,d = average(3,5)
#print(f"Media aritmetica este {c}, diferenta este{d}")
#print("inainte de inversare:",c,d)
'''
aux = c
c = d
d = aux
'''

#c,d = d,c
#print("dupa inversare",c,d)

#print(list_average([1]))
#print(verify_integer(2.7))

print(verify_integer(average(2,5)))